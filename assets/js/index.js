(function () {
    const RETURN_BUTTON = [10009, 27, 8],
        SELECT_BUTTON = 13,
        LEFT_ARROW_BUTTON = 37,
        UP_ARROW_BUTTON = 38,
        RIGHT_ARROW_BUTTON = 39,
        DOWN_ARROW_BUTTON = 40,
        detailContainer = document.getElementById(`detail-container`);

    var focusedId = 0,
        seriesCount = 0;

    const getInformation = async () => {
        const API_URL = 'https://content.viaplay.se/pc-se/serier/samtliga'; // Online API

        let response = await fetch(API_URL),
            result = await response.json();

        return result._embedded['viaplay:blocks'][0]._embedded['viaplay:products'];
    }

    const seriesList = () => {
        let outPut = '<div class="mt-5 mb-5 container"><div class="row">';

        const mainContainer = document.getElementById('main-container');

        getInformation().then(series => {
            seriesCount = series.length;
            series.map((item, index) => {
                const { content } = item;
                outPut +=
                    `<div class="col-sm-6 col-md-3 focusable series-item" id="series-box-${index}" tabindex="${index}">
                        <div class="series-item__inner">
                            <div class="series-item__img-box">
                                <img class="series-item__img" src=${content.images.landscape.url} alt=${content.series.title} />
                            </div>
                            <div class="series-item__title-box clearfix">
                                <div class="series-item__title">${content.series.title}</div>
                                <div class="series-item__year">${content.production.year}</div>
                            </div>
                        </div>
                    </div>`;
            })
            outPut += '</div></div>';

            mainContainer.innerHTML = outPut;
            document.getElementById(`series-box-${focusedId}`).focus();
            keyHandling(series);
        })
    }

    const keyHandling = series => {
        window.addEventListener('keydown', (e) => {
            switch (e.keyCode) {
                // Left
                case LEFT_ARROW_BUTTON:
                    if (focusedId > 0) focusedId--;
                    break;
                // Right
                case RIGHT_ARROW_BUTTON:
                    if (focusedId < seriesCount - 1) focusedId++;
                    break;
                // Top
                case UP_ARROW_BUTTON:
                    if (focusedId >= 4) focusedId -= 4;
                    break;
                // Bottom
                case DOWN_ARROW_BUTTON:
                    if (focusedId < seriesCount - 4) focusedId += 4;
                    break;
                // Enter
                case SELECT_BUTTON:
                    showDetailsContent(series[focusedId].content)
                    break;
                //Return
                default:
                    if (RETURN_BUTTON.includes(e.keyCode)) {
                        detailContainer.classList.remove('active');
                        detailContainer.innerHTML = '';
                    }
                    break;
            }

            document.getElementById(`series-box-${focusedId}`).focus();
        })
    }

    const showDetailsContent = (info) => {
        const detailsHTML =
            `
        <div class="series-details container">
                <div class="series-details__top row">
                    <div class="series-details__img-box col-md-9">
                        <div class="series-details__img-box">
                            <img class="series-details__img" src=${info.images.landscape.url} alt=${info.series.title} />
                        </div>
                    </div>
                    <div class="series-details__desc col-md-3">
                        <div class="series-details__desc-top">
                            <h1 class="series-details__title">
                                ${info.series.title}
                                <span class="series-details__year">(${info.production.year})</span>
                            </h1>
                            <div class="series-details__description">${info.series.synopsis}</div>
                        </div>
                            ${
            info.people && `
                                    <div class="series-details__people">
                                        ${
            info.people.actors && `
                                                <div class="series-details__people-row">
                                                    <span class="series-details__people-row-title">Actor ${info.people.actors.length > 2 ? 's' : ''}: </span>
                                                    ${info.people.actors.join(', ')}
                                                </div>`
            }
                                        ${
            info.people.directors && `
                                                <div class="series-details__people-row">
                                                    <span class="series-details__people-row-title">Director ${info.people.directors.length > 2 ? 's' : ''}: </span>
                                                    ${info.people.directors.join(', ')}
                                                </div>`
            }
                                        ${
            info.people.creators && `
                                                <div class="series-details__people-row">
                                                    <span class="series-details__people-row-title">Creator ${info.people.creators.length > 2 ? 's' : ''}: </span>
                                                    ${info.people.creators.join(', ')}
                                                </div>`
            }
                                    </div>`
            }
                            ${
            info.imdb && `
                                    <div class="series-details__imdb">
                                        <span class="series-details__imdb-title">IMDB:</span>
                                        <span class="series-details__imdb-rate"><strong>${info.imdb.rating}</strong>/10</span>
                                        <span class="series-details__imdb-votes">(${info.imdb.votes})</span>
                                    </div>`
            }
                    </div>
                </div>
            </div>`;

        detailContainer.innerHTML = detailsHTML;
        detailContainer.classList.add("active")

    }

    window.addEventListener('load', () => {
        seriesList();
    });
}())